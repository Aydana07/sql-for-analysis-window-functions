WITH RankedCustomers AS (
  SELECT
    s.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    ch.channel_desc,
    SUM(s.amount_sold) AS total_sales,
    RANK() OVER (ORDER BY SUM(s.amount_sold) DESC) AS overall_rank
  FROM
    sh.sales s
    JOIN sh.customers c ON s.cust_id = c.cust_id
    JOIN sh.channels ch ON s.channel_id = ch.channel_id
  WHERE
    EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2001)
  GROUP BY
    s.cust_id, c.cust_first_name, c.cust_last_name, ch.channel_desc
)

SELECT
  rc.cust_id,
  rc.cust_first_name,
  rc.cust_last_name,
  rc.channel_desc,
  rc.total_sales
FROM
  RankedCustomers rc
WHERE
  rc.overall_rank <= 300
ORDER BY
  rc.total_sales DESC, rc.channel_desc;

